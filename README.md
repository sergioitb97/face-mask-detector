This project aims to recognize people who are wearing a mask or not, the distinctive colors for recognition are green for the person who uses a mask and red for those who do not.

The project is divided into the following parts:

Dataset: contains 2 folders with images in JPG format of sample of people wearing masks and people without using them. All these images will be processed and used to train the model that will perform the subsequent recognition.

Face detector: Files used for the face-detection algorithm in the detect_mask_video file

train_mask_detector.py: Python file that will train the model using the images that we have added in the dataset section

detect_mask_video.py: Python file in charge of performing the recognition on an already trained model from the sample images.

mask_detector.model: Python model on which we will work

requirements.txt: Installation requirements file for the correct compatibility of the project

plot.png: Sample plot and its level of loss and precision
